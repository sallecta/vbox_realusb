# based on article Boot From a USB Drive in VirtualBox in Linux https://itsfoss.com/virtualbox-boot-from-usb/
# by Sagar Sharma https://itsfoss.com/author/sagar/

pthis="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

fn_stoponerror ()
{
	# Usage:
	# fn_stoponerror $BASH_SOURCE $LINENO $?
	from=$1
	line=$2
	error=$3
	if [[ $error -ne 0 ]]; then
		printf "\n$from: line $line: error: $error\n"
		exit $error
	fi
}

# trap ctrl-c and call ctrl_c()
trap fn_ctrl_c INT
fn_ctrl_c()
{
	printf "\n** $LINENO: Interrupted with CTRL-C\n"
	exit
}

# list usb drives
fn_listdrives ()
{
	echo 'Available drives: '
	for device in /sys/block/*
	do
		if udevadm info --query=property --path=$device | grep -q ^ID_BUS=usb
		then
			bndev="$(basename $device)"
			printf "\t $LINENO: "$bndev"\n"
		fi
	done
}
fn_listdrives
read -p "$LINENO: Select your drive"':' adrive

echo "$LINENO: Your drive is" $adrive

printf -v cur_min '%(%M%S)T' -1 
printf "$LINENO: Please confirm\n"
printf "\t"'Yes - "'$cur_min'"'"\n"
printf "\t"'No  -  Any other keys'"\n"
read -p "$LINENO: Type your answer"':' answer
if [[ "$answer" != "$cur_min" ]];then
	echo "$LINENO: Operation not confirmed"
	exit
else
	echo "$LINENO: Processing..."
fi

if [ ! -e "/dev/"$adrive ]; then
    echo "$LINENO: Error. Device ("$adrive") not exists!"
fi

vmdkname=$adrive".vmdk"
vmdkpath=$pthis"/user_data.ignore/drives_linked"

printf "$LINENO: Linked VMDK file is: $vmdkpath/$vmdkname\n"

GROUP="vboxusers"
if id -nG "$USER" | grep -qw "$GROUP"; then
	echo "$LINENO: User $USER belongs to $GROUP"
else
	echo "$LINENO: $USER does not belong to $GROUP, adding..."
	sudo usermod -a -G $GROUP $USER
	fn_stoponerror $BASH_SOURCE $LINENO $?
	echo "$LINENO: Warning! Please relogin!"
fi

GROUP="disk"
if id -nG "$USER" | grep -qw "$GROUP"; then
	echo "$LINENO: User $USER belongs to $GROUP"
else
	echo "$LINENO: $USER does not belong to $GROUP, adding..."
	sudo usermod -a -G $GROUP $USER
	fn_stoponerror $BASH_SOURCE $LINENO $?
	echo "$LINENO: Warning! Please relogin!"
	#newgrp $GROUP
	#fn_stoponerror $BASH_SOURCE $LINENO $?
	#newgrp $USER
	#fn_stoponerror $BASH_SOURCE $LINENO $?
fi

mkdir -p $vmdkpath
fn_stoponerror $BASH_SOURCE $LINENO $?

printf "\n$LINENO: Creating medium \n\n"
sudo VBoxManage createmedium disk --filename=$vmdkpath/$vmdkname --variant=RawDisk --format=VMDK --property RawDrive=/dev/$adrive
ecode=$?
printf "\n\n"

if [[ $ecode -ne 0 ]]; then
	printf "\n $LINENO: Executing: sudo VBoxManage closemedium disk $vmdkpath/$vmdkname\n"
	printf "\n $LINENO: Possible error: Medium exists. Removing Existing medium from VBox, please run me again.\n"
	sudo VBoxManage closemedium disk $vmdkpath/$vmdkname
	fn_stoponerror $BASH_SOURCE $LINENO $?
	sudo rm $vmdkpath/$vmdkname
	fn_stoponerror $BASH_SOURCE $LINENO $?
	exit
fi

sudo chmod 777 $vmdkpath/$vmdkname
fn_stoponerror $BASH_SOURCE $LINENO $?

sudo chown $USER $vmdkpath/$vmdkname
fn_stoponerror $BASH_SOURCE $LINENO $?

