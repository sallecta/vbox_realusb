# based on answer https://askubuntu.com/a/50290
# by Maxime R https://askubuntu.com/users/7567/maxime-r
# on article Mount a VirtualBox drive image (vdi)? https://askubuntu.com/questions/19430/mount-a-virtualbox-drive-image-vdi

pthis="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
fn_stoponerror ()
{
	# Usage:
	# fn_stoponerror $BASH_SOURCE $LINENO $?
	from=$1
	line=$2
	error=$3
	if [[ $error -ne 0 ]]; then
		printf "\n$from: line $line: error: $error\n"
		exit $error
	fi
}

fn_free ()
{
	printf "\n\t$LINENO: Unmounting all nbd0 parts\n"
	sudo umount /dev/nbd0p*
	printf "\n\t$LINENO: Disconnecting previous nbd disk\n"
	sudo qemu-nbd -d /dev/nbd0
	fn_stoponerror $BASH_SOURCE $LINENO $?
	printf "\n\t$LINENO: Unloading previous nbd module\n"
	sudo rmmod nbd
	fn_stoponerror $BASH_SOURCE $LINENO $?
	exit
}

sudo dpkg --status qemu qemu-utils > /dev/null
error=$?
if [[ $error -ne 0 ]]; then
	printf "\n$LINENO: This app is missing deps, attempting to install\n"
	sudo apt-get install qemu qemu-utils
	fn_stoponerror $BASH_SOURCE $LINENO $?
fi

printf "\n$LINENO: Loading the network block device module.\n"
ismod="$(lsmod | grep -o ^nbd)"
#echo ismod=$ismod

if [[ $1 == "free" ]]; then
	fn_free
fi
if [[ $ismod == "nbd" ]]; then
	fn_stoponerror $BASH_SOURCE $LINENO $?
	printf "\n\t$LINENO: Unloading previous nbd module\n"
	sudo rmmod nbd
	fn_stoponerror $BASH_SOURCE $LINENO $?
fi
sudo modprobe nbd max_part=16 > /dev/null
fn_stoponerror $BASH_SOURCE $LINENO $?

#pvdi="/dev/nbd0 drive.vdi"
#pvdi=$(dialog --title "Select a vdi file" --stdout --title "Please choose a vdi file to mount" --fselect $pthis 14 48)
pvdi=`zenity --file-selection --title="Select a VDI disk image file"`

if [ "$pvdi" = "" ] || [ ! -f $pvdi ]; then
	echo "File ($pvdi) not found!"
	exit
fi


printf "\n$LINENO: Attaching the vdi image [$pvdi]\n"
sudo qemu-nbd -c /dev/nbd0 $pvdi
fn_stoponerror $BASH_SOURCE $LINENO $?

printf "\n$LINENO: The vdi image [$pvdi] has been attached. Modern OS should automount its partitions\n"
printf "\n$LINENO: If not, try to mount it manually:\n"
printf "\t\n$LINENO: sudo mount /dev/nbd0p1 /some/mount/point\n"
#Now you will get a /dev/nbd0 block device, along with several /dev/nbd0p* partition device nodes.

# sudo mount /dev/nbd0p1 /mnt

# Once you are done, unmount everything and disconnect the device:

# sudo qemu-nbd -d /dev/nbd0
